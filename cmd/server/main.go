package main

import (
	"context"
	"encoding/json"
	"tcpserver/internal"
)

const (
	addr = ":8081"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	srv := internal.NewMessageServer(addr)

	srv.RegisterHandler("command/msg", func(ctx context.Context, msg []byte, executor *internal.Connection) {
		in := &internal.CmdMsg{}
		err := json.Unmarshal(msg, in)
		if err != nil {
			internal.Log("msg unmarshal error", err)
			return
		}

		// here just simple example
		if in.To == "all" {
			err = srv.BroadcastAll(msg)
		} else {
			err = srv.BroadcastByFilter(msg, func(labels internal.Labels) bool {
				return labels["name"] == in.To
			})
		}

		if err != nil {
			internal.Log("broadcast all error", err)
		}
	})

	srv.RegisterHandler("command/set-name", func(ctx context.Context, msg []byte, executor *internal.Connection) {
		in := &internal.CmdSetName{}
		err := json.Unmarshal(msg, in)
		if err != nil {
			internal.Log("set-name unmarshal error", err)
			return
		}

		executor.SetLabel("name", in.Name)
	})

	srv.OnAfterConnected(func(client *internal.Connection) {
		client.SetLabel("test", "test")
	})

	go func() {
		err := srv.Start(ctx)
		if err != nil {
			internal.Log("Server was stopped", err)
			cancel()
		}
	}()
	<-ctx.Done()
}
