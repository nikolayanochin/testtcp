package main

import (
	"bufio"
	"context"
	"os"
	"strings"
	"tcpserver/internal"
)

const (
	addr = ":8081"
)

// There are 3 simple commands that implemented in server/main.go (one of them is duplicated):
// command/msg: some-text-information-here (for all clients)
// command/set-name: my-name (for identity client)
// command/hello-msg-to: user-name (for say hello user by label)
func main() {
	ctx, _ := context.WithCancel(context.Background())
	client := internal.NewClient(addr)
	go client.Start(ctx)

	rd := bufio.NewReader(os.Stdin)
	go func() {
		// todo dirty code here. Need refactoring but no time now
		for {
			input, err := rd.ReadBytes('\n')
			if err != nil {
				internal.Log("read bytes err", err)
			}
			parts := strings.Split(string(input[:len(input)-1]), ": ")
			if len(parts) != 2 {
				internal.Log("error format, use: \"cmd-name: text\"")
				continue
			}

			cmdName := internal.CmdName(parts[0])
			text := parts[1]

			var cmd interface{}
			if cmdName == "command/msg" {
				cmd = &internal.CmdMsg{
					Msg: text,
					To:  "all",
				}
				err = client.SendCmd("command/msg", cmd)
			} else if cmdName == "command/hello-msg-to" {
				cmd = &internal.CmdMsg{
					Msg: "Hello!",
					To:  text,
				}
				err = client.SendCmd("command/msg", cmd)
			} else if cmdName == "command/set-name" {
				cmd = &internal.CmdSetName{
					Name: text,
				}
				err = client.SendCmd("command/set-name", cmd)
			} else {
				internal.Log("err exec cmd", err)
				continue
			}

			if err != nil {
				internal.Log("err exec cmd", err)
			}
		}
	}()

	<-ctx.Done()
}
