package internal

import (
	"context"
	"encoding/json"
	"fmt"
	"net"
)

type Client struct {
	Addr string
	conn net.Conn
}

// NewClient is simple client that can print input bytes only
// You can realize cmd functionality by server example
func NewClient(addr string) *Client {
	return &Client{Addr: addr}
}

// Start register client name
func (c *Client) Start(ctx context.Context) {
	conn, err := net.Dial("tcp", c.Addr)
	if err != nil {
		return
	}
	c.conn = conn

	buffer := make([]byte, 65535)
	go func() {
		for {
			_, err := readMessageBytes(conn, buffer)
			if err != nil {
				Log("read msg error", err)
				return
			}

			fmt.Println(string(buffer))
		}
	}()

	<-ctx.Done()
}

func (c *Client) SendCmd(cmdName CmdName, data interface{}) error {
	dataBytes, err := json.Marshal(data)
	if err != nil {
		return err
	}

	cmd := &command{
		Name: cmdName,
		Data: dataBytes,
	}

	cmdBytes, err := json.Marshal(cmd)
	if err != nil {
		return err
	}

	return writeMessageBytes(c.conn, cmdBytes)
}
