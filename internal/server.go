package internal

import (
	"bufio"
	"context"
	"encoding/binary"
	"encoding/json"
	"io"
	"net"
	"sync"
	"time"
)

type ConnectionCfg struct {
	ReadTimeout  time.Duration
	WriteTimeout time.Duration
}

type Hooks struct {
	AfterConnected func(*Connection)
}

type Server struct {
	Addr string

	hooks    *Hooks
	cfg      *ConnectionCfg
	active   bool
	listener net.Listener

	cMutex      sync.RWMutex
	connections connectionPool
	hMutex      sync.RWMutex
	handlers    map[CmdName]CmdProcessor
}

func NewMessageServer(addr string, cfgFns ...func(*ConnectionCfg)) *Server {
	defCfg := &ConnectionCfg{}
	for _, fn := range cfgFns {
		fn(defCfg)
	}
	return &Server{Addr: addr, cfg: defCfg, handlers: make(map[CmdName]CmdProcessor), hooks: &Hooks{}, connections: make(connectionPool)}
}

type CmdName string
type CmdProcessor func(ctx context.Context, msg []byte, executor *Connection)

func (s *Server) RegisterHandler(cmd CmdName, fn CmdProcessor) {
	s.hMutex.Lock()
	defer s.hMutex.Unlock()
	s.handlers[cmd] = fn
}

func (s *Server) Start(ctx context.Context) error {
	listener, err := net.Listen("tcp", s.Addr)
	if err != nil {
		Log("listener initialization failed", err)
		return err
	}
	s.listener = listener
	s.active = true

	go func() {
		for s.active {
			conn, err := listener.Accept()
			if err != nil {
				Log("accept connection failed :(", err)
				continue
			}

			s.configureConnection(conn)

			Log("connection accepted :)")
			go s.handleConnection(conn)
		}
	}()

	<-ctx.Done()
	return s.Stop()
}

func (s *Server) Stop() error {
	s.active = false
	time.Sleep(5 * time.Second)
	return s.listener.Close()
}

func (s *Server) OnAfterConnected(fn func(con *Connection)) {
	s.hooks.AfterConnected = fn
}

type connectionLabel interface{}

type Labels map[interface{}]interface{}

type Connection struct {
	sync.Mutex
	conn   net.Conn
	labels Labels
}

func (c *Connection) SetLabel(key interface{}, val interface{}) {
	c.labels[key] = val
}

func (s *Server) BroadcastByFilter(data []byte, fn func(Labels) bool) error {
	for _, cl := range s.connections {
		if !fn(cl.labels) {
			continue
		}

		err := s.BroadcastConnection(cl, data)
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *Server) BroadcastConnection(connection *Connection, data []byte) error {
	connection.Lock()
	defer connection.Unlock()
	return writeMessageBytes(connection.conn, data)
}

func (s *Server) BroadcastAll(data []byte) error {
	for _, cl := range s.connections {
		err := s.BroadcastConnection(cl, data)
		if err != nil {
			return err
		}
	}
	return nil
}

type connectionPool map[connectionLabel]*Connection

func (s *Server) configureConnection(conn net.Conn) {
	if s.cfg.ReadTimeout.Microseconds() > 0 {
		_ = conn.SetReadDeadline(time.Now().Add(s.cfg.ReadTimeout))
	}
	if s.cfg.WriteTimeout.Microseconds() > 0 {
		_ = conn.SetWriteDeadline(time.Now().Add(s.cfg.WriteTimeout))
	}
}

func (s *Server) getHandler(cmd CmdName) CmdProcessor {
	s.hMutex.RLock()
	defer s.hMutex.RUnlock()
	return s.handlers[cmd]
}

type command struct {
	Name CmdName `json:"name"`
	Data []byte  `json:"data"`
}

func (s *Server) handleConnection(netCon net.Conn) {
	con := s.registerConnection(netCon)

	buffer := make([]byte, 65535)
	for s.active {
		n, err := readMessageBytes(netCon, buffer)
		if err != nil {
			s.removeConnection(netCon)
			return
		}

		cmd := &command{}
		// todo proto buff here
		err = json.Unmarshal(buffer[:n], cmd)
		if err != nil {
			return
		}

		fn := s.getHandler(cmd.Name)
		if fn == nil {
			return
		}

		// todo we can generate context from input message if we need
		ctx := context.Background()
		go func() {
			fn(ctx, cmd.Data, con)
		}()
	}
}

func (s *Server) registerConnection(conn net.Conn) *Connection {
	label := getConnectionLabel(conn)
	cl := &Connection{
		conn:   conn,
		labels: make(Labels, 0),
	}

	s.cMutex.Lock()
	defer s.cMutex.Unlock()
	s.connections[label] = cl

	if s.hooks.AfterConnected != nil {
		s.hooks.AfterConnected(cl)
	}
	return cl
}

func (s *Server) removeConnection(conn net.Conn) {
	label := getConnectionLabel(conn)
	s.cMutex.Lock()
	defer s.cMutex.Unlock()
	delete(s.connections, label)
}

type defaultConnectionLabel struct {
	localAddr  string
	remoteAddr string
}

func getConnectionLabel(conn net.Conn) connectionLabel {
	return defaultConnectionLabel{
		localAddr:  conn.LocalAddr().String(),
		remoteAddr: conn.RemoteAddr().String(),
	}
}

func readMessageBytes(conn net.Conn, buffer []byte) (int, error) {
	reader := bufio.NewReader(conn)
	lenBuffer := make([]byte, 2)
	lenReadBytes := 0

	for lenReadBytes < 2 {
		ln, err := reader.Read(lenBuffer[lenReadBytes:])
		if err != nil {
			return 0, err
		}

		lenReadBytes += ln
		if lenReadBytes < 2 {
			continue
		}
	}

	msgLen := binary.BigEndian.Uint16(lenBuffer)
	return io.ReadAtLeast(reader, buffer, int(msgLen))
}

func writeMessageBytes(conn net.Conn, data []byte) error {
	prefixBytes := make([]byte, 2)
	binary.BigEndian.PutUint16(prefixBytes, uint16(len(data)))

	_, err := conn.Write(prefixBytes)
	if err != nil {
		return err
	}

	_, err = conn.Write(data)
	return err
}
