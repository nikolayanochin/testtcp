package internal

import "fmt"

func Log(msg string, errs ...error) {
	for i, err := range errs {
		if i == 0 {
			msg += ": "
		}
		msg += err.Error() + " "
	}
	fmt.Println(msg)
}

type CmdMsg struct {
	Msg string `json:"msg"`
	To  string `json:"to"`
}

type CmdSetName struct {
	Name string `json:"name"`
}
